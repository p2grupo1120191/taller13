#include <unistd.h>
#include <stdlib.h>
#include "../include/objetos.h"



void crear_Casa(void *ref, size_t tamano){
	Casa *buf = (Casa*)ref;
	buf->habitaciones=0;
	buf->puertas=0;
	buf->ventanas=0;
	
	buf->habitantes=5;
}

//Destructor
void destruir_Casa(void *ref, size_t tamano){
	Casa *buf = (Casa *)ref;
	buf->habitaciones=-1;
	buf->puertas=-1;
	buf->ventanas=-1;
	
	buf->habitantes=0;	
}
