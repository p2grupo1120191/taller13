/*Programacion Sistemas Grupo#11
	Taller#13
Integrantes:
	-Ariana Ochoa Ochoa
	-Erick Nunez Salazar*/
#include "slaballoc.h"
#include <stdio.h>
#include "objetos.h"
#include <string.h>
#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>

void *funcion_hilo(void * a);

typedef struct args
{
	SlabAlloc *cache;
	int id;
}args;

sem_t semaforo;

int main(int argc, char **argv) {

	sem_init(&semaforo, 0,1);

	if (argc != 3) {
		fprintf(stderr, "usage: %s -n <# de hilos>\n", argv[0]);
		exit(0);
	}

	int hilos;
	hilos = atoi(argv[2]);

	char *nombre="hola";
	size_t tamano_objeto=sizeof(objeto_ejemplo);	
	SlabAlloc *cache=crear_cache(nombre, tamano_objeto,crear_objeto_ejemplo,destruir_objeto_ejemplo);
	
	pthread_t threads[hilos];

	
	for(int i = 0; i < hilos; i++){

		args *args= malloc(sizeof(args));
		args-> id = i;
		args-> cache = cache;

		if(pthread_create(&threads[i], NULL, funcion_hilo, args)){
        	printf("\n ERROR creating thread 1");
        	exit(1);
    	}

	}

	for(int i = 0; i < hilos; i++){

		pthread_join(threads[i], NULL);
	}

}

void * funcion_hilo(void * a){

	args *arg = (args *)a;
	
	void *objetos[1000];
	
	for(int i = 0; i < 1000; i++){
		
		sem_wait(&semaforo);
		void * obj = obtener_cache(arg->cache, 1);
		sem_post(&semaforo);

		objeto_ejemplo *cast = (objeto_ejemplo *)obj;
		cast -> id_hilo = arg->id;
        cast->data[0] = 0 * arg->id;
        cast->data[1] = 1 * arg->id;
        cast->data[2] = 2 * arg->id;

		objetos[i] = obj;

	}


	for(int i = 700; i < 1000; i++){
		sem_wait(&semaforo);
		devolver_cache(arg->cache, objetos[i]);
		sem_post(&semaforo);
	}

	
	for(int i = 0; i < 700; i++){
		objeto_ejemplo * cast = objetos[i];
		if(cast->id_hilo != arg->id || cast->data[0] != 0 * arg->id || cast->data[1] != 1 * arg->id || cast->data[2] != 2 * arg->id ){
			printf("Esperado: id_hilo\n %d, data[0]: %d, data[1]: %d, data[2]: %d\n", arg->id, arg->id * 0, arg->id * 1, arg->id * 2);
			printf("Valores econtrados\n id_hilo: %d, data[0]: %d, data[1]: %d, data[2]: %d", cast->id_hilo, cast->data[0], cast->data[1], cast->data[2]);
		}
	}

	free(arg);
	return (void*)0;
}
