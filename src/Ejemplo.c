#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_Ejemplo(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = 0;
    int i = 0;
    for(i = 0; i< 100; i++){
    	buf->b[i] = i;
    }
    buf->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Ejemplo(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = -1;
    //buf->b = 0.0f;
    free(buf->msg);
}

//Implementacion nuevas funciones
void crear_objeto_ejemplo(void *ref, size_t tamano){
    objeto_ejemplo *buf = (objeto_ejemplo *)ref;
    buf->data = malloc(3 * sizeof(int));

}

void destruir_objeto_ejemplo(void *ref, size_t tamano){
    objeto_ejemplo *buf = (objeto_ejemplo *)ref;
    buf->id_hilo = -1;
    //free(buf->data);
}
